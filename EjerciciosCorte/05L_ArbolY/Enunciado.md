# Ejercicio 5

## Aprendizaje a través de explicaciones previas

 Esta es la historia de un hombre nervioso a quien la enfermedad le había agudizado sus sentidos,  que está obsesionado con el ojo de un anciano a quien quiere mucho y con el cual convive. Este ojo lo perturba al punto de querer liberarse de dicho ojo para siempre.

Tomando como base el cuento “El corazón delator” de Edgar Allan Poe, explique por qué es posible que el hombre confiese su crimen ante la policía cuando ni siquiera sospechaban de él.

Cree el árbol Y con los patrones generalizados para el cuento.  Extraiga las reglas correspondientes a las implicaciones dentro del árbol y verifique si es posible obtener la explicación solicitada.

Resultados: Archivo EjercicioNo5.pdf
