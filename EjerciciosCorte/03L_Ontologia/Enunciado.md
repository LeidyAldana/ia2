# Ontología

Descripción formal de conceptualizaciones compartidas, usadas para representar conocimiento. "Todo lo que existe puede ser representado"

## Ejercicio

Con el fin de crear una herramienta que dé información sobre los posgrados de ingeniería en los países de la Alianza del Pacífico (Colombia, México, Perú y Chile), se requiere crear una Ontología que tenga en cuenta la unificación de conceptos y homologación de contenidos en los posgrados de los países mencionados, además de los agrupamientos por áreas temáticas, facilidades de acceso y tipos de posgrado (profundización o investigación).
Usando Protègé, construya la ontología sobre los posgrados, mencionada antes, que incluya las diversas características de los diferentes conceptos. Tenga en cuenta solamente dos carreras universidades en cada país.  Debe ser posible identificar mínimo 20 características y se deben incluir los principales conceptos de ontologías (cuya explicación encuentran en el documento que incluye el ejemplo de la pizza):
individuos o instancias
propiedades:
    - de anotación
    -  de tipo de dato,o    de tipo de objeto: inversa, funcional,inversa-funcional.o    transitivao    simétricao    asimétricao    reflexivao    irreflexivaRestriccioneso    De cuantificación: Existenciales, universales, axioma de cierreo    De cardinalidad: al menos, como máximo, exactamenteo    tiene valorClases:o    disjuntaso    primitivaso    definidaso    enumeradaso    equivalentesUso del razonador

## Análisis

- Unificación de conceptos y homologación de contenidos
- Agrupación por area tematica, facilidad de acceso y tipo de posgrado (profundización o investigación)
- 2 carreras en universidades de cada país
- Mínimo 20 caracterísiticas
- Incluir conceptos de ontologías:

individuos o instancias
	
 propiedades: 

o    de anotación: añaden metadata para clases, individuos y propiedades de objetos y tipos de datos 
o    de tipo de dato, 
o    de tipo de objeto: inversa, funcional,           

inversa-funcional: Cada obj tiene 1 sola.

o    transitiva: Si a->b->c entonces a->c , Si tipo->pertenece a->Facultad->establece->Jornada entonces tipo->tiene->jornada
o    simétrica: La misma propiedad aplica en ambos sentidos
o    asimétrica: Una propiedad aplica en 1 sentido Ej: Tipo pertenece a Facultad, la Facultad NO puede pertenecer a 1 tipo
o    reflexiva: Se puede aplicar a la misma clase y a otras Ej: Facultad establece Facultad
o    irreflexiva: Si a se relaciona con b, entonces a es diferente de b Ej: Tipo pertenece a Facultad pero Tipo no pertenece a Tipo
	

Restricciones

o    De cuantificación: (No se colocaron por la versión de Protege)
	- Existenciales: describe clases de individuos que participan en AL MENOS 1 relación (No se colocaron por la versión de Protege)
	- universales: describe clases de individuos que para una propiedad dada solo tiene relaciones entre esta propiedad para individios que son miembros de una clase en específico
	- axioma de cierre

o    De cardinalidad: al menos, como máximo, exactamente
o    tiene valor

 Clases:

o    disjuntas: Un individuo no puede ser una instancia de más de 1 de esas 3 clases, (Maestría, diplomado o doctorado)
o    primitivas
o    definidas
o    enumeradas
o    equivalentes

Uso del razonador

### Clases


