% -------------------------------------------------------- %
% Inteligencia Artificial II - 2019-3                      %
%                                                          %
% Estudiantes:                                             %
% Leidy Marcela Aldana - 20151020019                       %
% Andrés Mauricio Acosta Pulido - 20142020111              %
% Fecha: Octubre de 2019                                   %
% -------------------------------------------------------- %

clear all
close all

% -------------------------------------------------------- %
% Objetivo:                                                %
% 3. Aplicar los filtros Kernel enfoque, desenfoque,       %
% repujado, etc. vistos en clase                           %
% -------------------------------------------------------- %

% Se declara la matriz que contiene el Kernel y ejercerá el cambio en la
% imagen, dependiendo del efecto que se quiera obtener
% 1. Enfoque
kernel1=[0 -1 0; -1 5 -1; 0 -1 0];
% 2. Desenfoque
kernel2=[1 1 1; 1 1 1; 1 1 1];
% 3. Realzar bordes
kernel3=[0 0 0; -1 1 0; 0 0 0];
% 4. Detecta bordes
kernel4=[0 1 0; 1 -4 1; 0 1 0];
% 5. Repujado
kernel5=[-2 -1 0; -1 1 1; 0 1 2];
% 6. Mascara adicional --> Filtro de tipo Sharpen
kernel6=[1 -2 1; -2 5 -2; 1 -2 1];

%Sharpening --> Agudiazamiento, afilamiento
% Técnica para aumentar nitidez 

% El tamaño de las imagenes es multiplo de 3, así que se multiplica por los
% diferentes Kernel de tamaño 3x3

%Cargando la imagen
fig = imread('hunter.jpg');
figd = double(fig)/255;
figure('Name','Original')
imshow(figd)

%Filtrado
figdbw = rgb2gray(figd);

figfx = multiplicarMatrizB(figdbw,kernel1);
figure('Name','Caricatura Kernel1, enfoque')
imshow(figfx)


figfx = multiplicarMatrizB(figdbw,kernel2);
figure('Name','Caricatura Kernel2, desenfoque')
imshow(figfx)


figfx = multiplicarMatrizB(figdbw,kernel3);
figure('Name','Caricatura Kernel3, realza bordes')
imshow(figfx)


figfx = multiplicarMatrizB(figdbw,kernel4);
figure('Name','Caricatura Kernel4, detecta bordes')
imshow(figfx)


figfx = multiplicarMatrizB(figdbw,kernel5);
figure('Name','Caricatura Kernel5, repuja')
imshow(figfx)

tic
figfx = multiplicarMatrizB(figdbw,kernel6);
figure('Name','Caricatura Kernel5, Autoría propia')
imshow(figfx)
toc


function B = multiplicarMatrizB(A, k)
    [r, c] = size(A);
    [m, n] = size(k);
    h = rot90(k, 2);
    center = floor((size(h)+1)/2);
    left = center(2) - 1;
    right = n - center(2);
    top = center(1) - 1;
    bottom = m - center(1);
    Rep = zeros(r + top + bottom, c + left + right);
    for x = 1 + top : r + top
      for y = 1 + left : c + left
          Rep(x,y) = A(x - top, y - left);
      end
    end
    B = zeros(r , c);
    for x = 1 : r
      for y = 1 : c
          for i = 1 : m
              for j = 1 : n
                  q = x - 1;
                  w = y -1;
                  B(x, y) = B(x, y) + (Rep(i + q, j + w) * h(i, j));
              end
          end
      end
    end
end

