# Visión por Computador

Busque otro método de detección de bordes en una imagen (diferente de los vistos en clase) describa detalladamente (usando teoría y ejemplos) el proceso que sigue este algoritmo para hallar los bordes. Implemente en Mathlab los tres algoritmos (los dos vistos en clase y el que usted eligió). Haga un análisis comparativo acerca del comportamiento y efectividad de estos tres algoritmos, determinando según su criterio y resultados obtenidos, un ranking de efectividad entre los tres algoritmos.

## Analisis punto 2

Algoritmos usados:

- Filtro líneal (-1 1 y -1; 1) -- _Primera Derivada_
 después de unen con: sqrt(abs(figfx.^2)+abs(figfy.^2))
	Tiempo empleado: 0.937536 sg

- Filtro con (1 -2 1 y 1; -2; 1) -- _Segunda Derivada_
 y después se unen con la tangente inversa de 4 cuadrantes
	Tiempo empleado: 0.897048 sg

## Analisis punto 3

Kernel usados:

- % 1. Enfoque
kernel1=[0 -1 0; -1 5 -1; 0 -1 0];
Tiempo: 0.463307 sg

- % 2. Desenfoque
kernel2=[1 1 1; 1 1 1; 1 1 1];
Tiempo: 0.396608 sg

- % 3. Realzar bordes
kernel3=[0 0 0; -1 1 0; 0 0 0];
Tiempo: 0.346539 sg

- % 4. Detecta bordes
kernel4=[0 1 0; 1 -4 1; 0 1 0];
Tiempo: 0.488114 sg

- % 5. Repujado
kernel5=[-2 -1 0; -1 1 1; 0 1 2];
Tiempo: 0.296660 sg

- % 6. Mascara adicional --> Filtro de tipo Sharpen
kernel6=[1 -2 1; -2 5 -2; 1 -2 1];
Tiempo: 0.302323 sg
