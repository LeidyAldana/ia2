close all 
clear all
% 9. Defina 3 modelos de redes neuronales y pruebe su funcionamiento con el archivo de ejemplos numéricos
% elegido para el ejercicio 6 del árbol KD. Haga un análisis comparativo acerca de la efectividad de cada
% una de las redes neuronales que definió.
data=readmatrix('default_of_credit_card_clients.xls');
data=data.';
P=data(1:24,:);
T=data(25:25,:);


%Primera forma
fact={'logsig','hardlim'};
net1 = newff(P,T,[2 20], fact);
net1=train(net1,P,T);
salida=sim(net1,P);

%Segunda forma
valorMax=max(P);
valorMin=min(P);
valorMax=valorMax.';
valorMin=valorMin.';
totalValoresMaxMin=[valorMin valorMax];
net2 = newp(P,T);
net2.adaptParam.passes = 11;
net2=adapt(net2, P,T);

%Tercera Forma
net3=newp(P,T);
net3.b{1}=0.5;
net3=train(net3,P,T);

%Pruebas
prueba=data(1:24,1:100);
pruebaNet1=sim(net1,prueba);
pruebaNet2=sim(net2,prueba);
pruebaNet3=sim(net3,prueba);

