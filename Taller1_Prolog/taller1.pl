padre(edmundotudor,enrique7).
padre(enrique7,enrique8).
padre(enrique7,arturo).
padre(enrique7,margarita).
padre(enrique7,maria).
padre(enrique8,maria1).
padre(enrique8,eduardo6).
padre(enrique8,isabel1).
padre(carlos,francisca).
padre(enrique,juana1grey).
padre(eduardo3,eduardo).
padre(eduardo3,juan).
padre(eduardo3,edmundo).
padre(eduardo,ricardo2).
padre(juan,enrique4).
padre(juan,margarita).
padre(enrique4,enrique5).
padre(enrique5,enrique6).
padre(edmundo,ricardo).
padre(ricardo,eduardo4).
padre(eduardo4,isabel).
padre(eduardo4,eduardo5).
padre(ricardo,ricardo3).

madre(margarita,enrique7).
madre(isabel,enrique8).
madre(isabel,arturo).
madre(isabel,margarita).
madre(isabel,maria).
madre(catalina,maria1).
madre(juana,eduardo6).
madre(ana,isabel1).
madre(maria,francisca).
madre(francisca,juana1grey).

hombre(enrique7).
hombre(edmundotudor).
hombre(eduardo6).
hombre(carlos).
hombre(enrique8).
hombre(arturo).
hombre(eduardo6).
hombre(eduardo3).
hombre(juan).
hombre(eduardo).
hombre(edmundo).
hombre(enrique4).
hombre(enrique5).
hombre(ricardo).
hombre(eduardo4).
hombre(eduardo5).
hombre(ricardo3).

mujer(margarita).
mujer(maria1).
mujer(maria).
mujer(francisca).
mujer(ana).
mujer(catalina).
mujer(juana1grey).
mujer(isabel1).
mujer(isabel).

es_abuelo(X,Y):-(padre(X,C),padre(C,Y)),hombre(X).
es_abuela(X,Y):-mujer(X),(madre(X,C),madre(C,Y);madre(X,C),padre(C,Y)). 
son_hermanos(X,Y):-((padre(Z,X),padre(Z,Y));(madre(C,X),madre(C,Y))), X\=Y.
es_tia(X,Y):-mujer(X),(padre(Z,Y),son_hermanos(Z,X));(madre(C,Y),son_hermanos(C,X)).
es_tio(X,Y):-hombre(X),(padre(Z,Y),son_hermanos(Z,X));(madre(C,Y),son_hermanos(C,X)).
es_nieto(X,Y):-((hombre(X),padre(Y,C),padre(C,X));(hombre(X),padre(Y,C),madre(C,X));(hombre(X),madre(Y,C),padre(C,X))).
es_nieta(X,Y):-((mujer(X),padre(Y,C),padre(C,X));(mujer(X),padre(Y,C),madre(C,X));(mujer(X),madre(Y,C),padre(C,X))).
es_primo_hermano(X,Y):-es_nieto(X,Z),es_nieto(Y,Z), not(son_hermanos(X,Y)).
es_prima_hermano(X,Y):-es_nieta(X,Z),es_nieta(Y,Z), not(son_hermanos(X,Y)).
es_bisabuelo(X,Y):-es_abuelo(X,C),es_abuelo(C,Y).
es_bisabuela(X,Y):-es_abuela(X,C),es_abuela(C,Y).
