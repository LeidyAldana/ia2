padre(pedro,jose).
padre(jose,luis).
padre(jose,ignacio).
padre(ignacio,maria).
padre(ignacio,mercedes).
ancestro(X,Y):- padre(X,Y).
ancestro(X,Y):- padre(X,C),ancestro(C,Y).