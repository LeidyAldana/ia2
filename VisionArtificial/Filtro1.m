%Andrés Mauricio Acosta Pulido 20142020111
%Leidy Marcela Aldana Burgos 20151020019
clear all
close all

%Cargando la imagen
%fig = imread('flor.jpg');
%fig = imread('sao.jpeg');
fig = imread('hunter.jpg');

figd = double(fig)/255;
imshow(figd);

%Filtrado
figdbw = rgb2gray(figd);
imshow(figdbw);
size(figdbw) ; 
%figdbw=[12 18 6; 2 1 7; 100 140 130]
%imshow(figdbw)
Hx = [-1 1];
Hy = [-1;1];

figfx = filter2(Hx,figdbw);

figfy = filter2(Hy,figdbw);

%Total
figft = sqrt(abs(figfx.^2)+abs(figfy.^2));
imshow(figft)
