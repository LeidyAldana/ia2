
%Andrés Mauricio Acosta Pulido 20142020111
%Leidy Marcela Aldana Burgos 20151020019
clear all
close all

%Cargando la imagen
%fig = imread('flor.jpg');
%fig = imread('sao.jpeg');
fig = imread('hunter.jpg');

figd = double(fig)/255;
imshow(figd)

%Filtrado
figdbw = rgb2gray(figd);
imshow(figdbw)
size(figdbw)    

Hx = [0 1 0;0 0 0;0 0 0]
Hy = [0 1 0;0 0 0; 0 0 0]   

figfx = filter2(Hx,figdbw);
imshow(abs(figfx))

figfy = filter2(Hy,figdbw);
imshow(abs(figfy))

%Total
figft = sqrt(abs(figfx.^2)+abs(figfy.^2));
imshow(figft)


