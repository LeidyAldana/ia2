%Andrés Mauricio Acosta Pulido 20142020111
%Leidy Marcela Aldana Burgos 20151020019
clear all
close all

%Cargando la imagen
fig = imread('sao.jpeg');
figd = double(fig)/255;
figure('Name','original')
imshow(figd)

%Filtrado
figdbw = rgb2gray(figd);
Hx = [-1 1];
Hy = [-1;1];
figfx = multiplicarMatriz(figdbw,Hx);
figure('Name','horizontal')
imshow(figfx)

figfy = multiplicarMatriz(figdbw,Hy);
figure('Name','vertical')
imshow(figfy)

%Total
figft = sqrt(abs(figfx.^2)+abs(figfy.^2));
figure('Name','total')
imshow(figft)

function B = multiplicarMatriz(A, k)
[r, c] = size(A);
[m, n] = size(k);
h = rot90(k, 2);
center = floor((size(h)+1)/2);
left = center(2) - 1;
right = n - center(2);
top = center(1) - 1;
bottom = m - center(1);
Rep = zeros(r + top + bottom, c + left + right);
for x = 1 + top : r + top
  for y = 1 + left : c + left
      Rep(x,y) = A(x - top, y - left);
  end
end
B = zeros(r , c);
for x = 1 : r
  for y = 1 : c
      for i = 1 : m
          for j = 1 : n
              q = x - 1;
              w = y -1;
              B(x, y) = B(x, y) + (Rep(i + q, j + w) * h(i, j));
          end
      end
  end
end
end

