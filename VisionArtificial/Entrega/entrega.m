% -------------------------------------------------------- %
% Inteligencia Artificial II - 2019-3                      %
% Taller de manejo de imágenes                             %
% Estudiantes:                                             %
% Leidy Marcela Aldana - 20151020019                       %
% Andrés Mauricio Acosta Pulido - 20142020111              %
% Fecha: Jueves 26 de Septiembre de 2019                   %
% -------------------------------------------------------- %

clear all
close all

% -------------------------------------------------------- %
% Objetivo:                                                %
% 1. Tomar una foto real y una caricatura                  %
% -------------------------------------------------------- %

figCat = imread('cat.jpeg');
fig = imread('sao.jpeg');

% Se convierte la imagen a escala de grises
figdbwCat = rgb2gray(figCat);
figdbw = rgb2gray(fig);
% El tamaño no muestra la tercera componente, 
% por lo cual esta a blanco y negro
size(figdbwCat)
size(figdbw)
% Se muestra la imagen para corroborar la hispotesis anterior
figure
imshow(figdbwCat);
figure
imshow(figdbw);

% -------------------------------------------------------- %
% Objetivo:                                                %
% 2. Hallar los bordes verticales, los bordes horizontales %
% y la matriz correspondiente a la unión de los dos bordes %
% -------------------------------------------------------- %

% Archivos:
% punto2Caricatura y punto2Imagen

% -------------------------------------------------------- %
% Objetivo:                                                %
% 3. Aplicar los filtros Kernel enfoque, desenfoque,       %
% repujado, etc. vistos en clase                           %
% -------------------------------------------------------- %

% Se declara la matriz que contiene el Kernel y ejercerá el cambio en la
% imagen, dependiendo del efecto que se quiera obtener
% 1. Enfoque
kernel1=[0 -1 0; -1 5 -1; 0 -1 0];
% 2. Desenfoque
kernel2=[1 1 1; 1 1 1; 1 1 1];
% 3. Realzar bordes
kernel3=[0 0 0; -1 1 0; 0 0 0];
% 4. Detecta bordes
kernel4=[0 1 0; 1 -4 1; 0 1 0];
% 5. Repujado
kernel5=[-2 -1 0; -1 1 1; 0 1 2];

% El tamaño de las imagenes es multiplo de 3, así que se multiplica por los
% diferentes Kernel de tamaño 3x3

%Cargando la imagen
fig = imread('sao.jpeg');
figd = double(fig)/255;
figure('Name','Original')
imshow(figd)

%Filtrado
figdbw = rgb2gray(figd);

figfx = multiplicarMatrizB(figdbw,kernel1);
figure('Name','Caricatura Kernel1, enfoque')
imshow(figfx)

figfx = multiplicarMatrizB(figdbw,kernel2);
figure('Name','Caricatura Kernel2, desenfoque')
imshow(figfx)

figfx = multiplicarMatrizB(figdbw,kernel3);
figure('Name','Caricatura Kernel3, realza bordes')
imshow(figfx)

figfx = multiplicarMatrizB(figdbw,kernel4);
figure('Name','Caricatura Kernel4, detecta bordes')
imshow(figfx)


figfx = multiplicarMatrizB(figdbw,kernel5);
figure('Name','Caricatura Kernel5, repuja')
imshow(figfx)





%Cargando la imagen
fig = imread('cat.jpeg');
figd = double(fig)/255;
figure('Name','Original')
imshow(figd)

%Filtrado
figdbw = rgb2gray(figd);

figfx = multiplicarMatrizB(figdbw,kernel1);
figure('Name','Caricatura Kernel1, enfoque')
imshow(figfx)

figfx = multiplicarMatrizB(figdbw,kernel2);
figure('Name','Caricatura Kernel2, desenfoque')
imshow(figfx)

figfx = multiplicarMatrizB(figdbw,kernel3);
figure('Name','Caricatura Kernel3, realza bordes')
imshow(figfx)

figfx = multiplicarMatrizB(figdbw,kernel4);
figure('Name','Caricatura Kernel4, detecta bordes')
imshow(figfx)


figfx = multiplicarMatrizB(figdbw,kernel5);
figure('Name','Caricatura Kernel5, repuja')
imshow(figfx)
% -------------------------------------------------------- %
% Objetivo:                                                %
% Funcion creada para multiplicar dos matrices de          %
% dimensiones M1(N*M)M2(M*L)                               %
% -------------------------------------------------------- %

function producto1=multiplicarMatriz(matriz2, matriz1)
    N=3; L=3; M=3;
    producto=zeros(N,L);
    for i=1:N
        disp('i')
        for j=1:L
            producto(i,j)=matriz1(i,1)*matriz2(1,j);
            for k=2:M
                producto(i,j)=producto(i,j)+matriz1(i,k)*matriz2(k,j);
            end
        end
    end
    producto1=producto;
    disp(producto1)
end

% -------------------------------------------------------- %
% Objetivo:                                                %
% Funcion creada para sobreponer el kernel sobre una matriz%
% -------------------------------------------------------- %

function product=sobreponer(matriz1, kernel)
    disp('Sobreposición')
    disp(matriz1)
    disp(kernel)
    N=3; L=3; M=3;
    producto=zeros(N,L);
    anterior=0;
    aux=0;
    variable=0;
    for i=1:N
        for j=1:L
            disp('dentro del ciclo')
            disp(matriz1(i,j))
            disp(kernel(i,j))
            variable=times(matriz1(i,j),kernel(i,j))
            disp('op')
            producto(i,j)=matriz1(i,j).*kernel(i,j)
            disp(producto)
            aux=producto(i,j)+aux
        end
        anterior=anterior+aux
    end
    product=anterior;
    disp('producto es')
    disp(product)
end

% -------------------------------------------------------- %
% Objetivo:                                                %
% Funcion creada enviar a sobreponer el kernel en cierta sección de la matriz %
% -------------------------------------------------------- %

function fct=obtenerMod(imagen, kernel)
    mbn=rgb2gray(imagen);
    [x,y]=size(mbn);
    for i=2:x
        for j=2:y
            disp('i,j == ')
            disp(i)
            disp(j)
            if i>=400 || j>=400
                   matrizM=eye(3)
            else
                   %matriz3=[ mbn(i,j) mbn(i, j+1) mbn(i, j+2); mbn(i+1,j) mbn(i+1,j+1) mbn(i+1,j+2);  mbn(i+2,j) mbn(i+2,j+1) mbn(i+2,j+2) ]
                   matrizM=[ mbn(i-1,j-1) mbn(i-1, j) mbn(i-1, j+1); mbn(i,j-1) mbn(i,j) mbn(i,j+1);  mbn(i+2,j-1) mbn(i+2,j) mbn(i+2,j+1) ]
                   resultado=sobreponer(matrizM, kernel) %disp(mbn(i,j))
                   matrizNueva(i,j)=resultado;
            end               
        end

    end
    fct=matrizNueva
end



function B = multiplicarMatrizB(A, k)
    [r, c] = size(A);
    [m, n] = size(k);
    h = rot90(k, 2);
    center = floor((size(h)+1)/2);
    left = center(2) - 1;
    right = n - center(2);
    top = center(1) - 1;
    bottom = m - center(1);
    Rep = zeros(r + top + bottom, c + left + right);
    for x = 1 + top : r + top
      for y = 1 + left : c + left
          Rep(x,y) = A(x - top, y - left);
      end
    end
    B = zeros(r , c);
    for x = 1 : r
      for y = 1 : c
          for i = 1 : m
              for j = 1 : n
                  q = x - 1;
                  w = y -1;
                  B(x, y) = B(x, y) + (Rep(i + q, j + w) * h(i, j));
              end
          end
      end
    end
end
