P=[0.6 -0.2 -0.3 0.1 0.5; 0.2 0.9 0.4 0.1 -0.6];
T=[1 1 0 0 0];

plotpv(P,T)
% [MIN, MAX]
red=newp([-0.3 0.6; -0.6 0.9], 1);
red.iw{1,1} = [1,1];
red.b{1}=0.5;
pesos=red.iw{1,1};
bias=red.b{1};
plotpc(pesos,bias);
red=train(red,P,T);
figure;
pesos=red.iw{1,1};
bias=red.b{1};
plotpv(P,T);
plotpc(pesos,bias);

%--------------------------
prueba=[-0.56; 0.1];
a=sim(red,prueba)

%OTRA FORMA DE ENTRENAR LA RED NEURONAL
net = newp([-0.3 0.6; -0.6 0.9], 1);
net.adaptParam.passes = 11;
net=adapt(net, P,T);
plotpc(net.iw{1},net.b{1});
prueba=[-0.56; 0.1];
a=sim(net,prueba)